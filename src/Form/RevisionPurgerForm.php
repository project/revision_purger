<?php

namespace Drupal\revision_purger\Form;

use Drupal\comment\CommentManagerInterface;
use Drupal\Core\Entity\ContentEntityTypeInterface;
use Drupal\Core\Entity\EntityForm;
use Drupal\Core\Entity\EntityManagerInterface;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Form\FormStateInterface;
use Psr\Log\LoggerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Base form handler for revision purger edit forms.
 *
 * @internal
 */
class RevisionPurgerForm extends EntityForm {

  /**
   * Entity manager service.
   *
   * @var \Drupal\Core\Entity\EntityManagerInterface
   */
  protected $entityManager;

  /**
   * A logger instance.
   *
   * @var \Psr\Log\LoggerInterface
   */
  protected $logger;

  /**
   * The comment manager.
   *
   * @var \Drupal\comment\CommentManagerInterface
   */
  protected $commentManager;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('entity.manager'),
      $container->get('logger.factory')->get('revision_purger'),
      $container->get('comment.manager')
    );
  }

  /**
   * Constructs a CommentTypeFormController
   *
   * @param \Drupal\Core\Entity\EntityManagerInterface $entity_manager
   *   The entity manager service.
   * @param \Psr\Log\LoggerInterface $logger
   *   A logger instance.
   * @param \Drupal\comment\CommentManagerInterface $comment_manager
   *   The comment manager.
   */
  public function __construct(EntityManagerInterface $entity_manager, LoggerInterface $logger, CommentManagerInterface $comment_manager) {
    $this->entityManager = $entity_manager;
    $this->logger = $logger;
    $this->commentManager = $comment_manager;
  }

  /**
   * {@inheritdoc}
   */
  public function form(array $form, FormStateInterface $form_state) {
    $form = parent::form($form, $form_state);

    /** @var \Drupal\revision_purger\Entity\RevisionPurger $revision_purger */
    $revision_purger = $this->entity;

    $form['label'] = [
      '#type' => 'textfield',
      '#title' => t('Label'),
      '#maxlength' => 255,
      '#default_value' => $revision_purger->label(),
      '#required' => TRUE,
    ];
    $form['id'] = [
      '#type' => 'machine_name',
      '#default_value' => $revision_purger->id(),
      '#machine_name' => [
        'exists' => '\Drupal\revision_purger\Entity\RevisionPurger::load',
      ],
      '#maxlength' => EntityTypeInterface::BUNDLE_MAX_LENGTH,
      '#disabled' => !$revision_purger->isNew(),
    ];

    $form['description'] = [
      '#type' => 'textarea',
      '#default_value' => $revision_purger->getDescription(),
      '#description' => t('Describe this purger. The text will be displayed on the <em>Revision purgers</em> administration overview page.'),
      '#title' => t('Description'),
    ];

    if ($revision_purger->isNew()) {
      $options = [];
      foreach ($this->entityTypeManager->getDefinitions() as $entity_type) {
        if ($entity_type instanceof ContentEntityTypeInterface && $entity_type->isRevisionable()){
          $options[$entity_type->id()] = $entity_type->getLabel();
        }
      }
      $form['target_entity_type_id'] = [
        '#type' => 'select',
        '#default_value' => $revision_purger->getTargetEntityTypeId(),
        '#title' => t('Target entity type'),
        '#options' => $options,
        '#required' => TRUE,
        '#description' => t('The target entity type.'),
      ];
    }
    else {
      $form['target_entity_type_id_display'] = [
        '#type' => 'item',
        '#markup' => $this->entityManager->getDefinition($revision_purger->getTargetEntityTypeId())->getLabel(),
        '#title' => t('Target entity type'),
      ];
    }
    $form['count'] = [
      '#type' => 'textfield',
      '#default_value' => $revision_purger->getCount(),
      '#description' => t('Delete after number of revisions.'),
      '#title' => t('Revision count'),
    ];
    $form['interval'] = [
      '#type' => 'textfield',
      '#default_value' => $revision_purger->getInterval(),
      '#description' => t('Delete after given time.'),
      '#title' => t('Revision time'),
    ];


    $form['actions'] = ['#type' => 'actions'];
    $form['actions']['submit'] = [
      '#type' => 'submit',
      '#value' => t('Save'),
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function save(array $form, FormStateInterface $form_state) {
    $revision_purger = $this->entity;
    $status = $revision_purger->save();

    $edit_link = $this->entity->link($this->t('Edit'));
    if ($status == SAVED_UPDATED) {
      $this->messenger()->addStatus(t('Revision purger %label has been updated.', ['%label' => $revision_purger->label()]));
      $this->logger->notice('Revision purger %label has been updated.', ['%label' => $revision_purger->label(), 'link' => $edit_link]);
    }
    else {
      // $this->commentManager->addBodyField($revision_purger->id());
      $this->messenger()->addStatus(t('Revision purger %label has been added.', ['%label' => $revision_purger->label()]));
      $this->logger->notice('Revision purger %label has been added.', ['%label' => $revision_purger->label(), 'link' => $edit_link]);
    }

    $form_state->setRedirectUrl($revision_purger->urlInfo('collection'));
  }

}
