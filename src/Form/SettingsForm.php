<?php

namespace Drupal\revision_purger\Form;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\media\IFrameUrlHelper;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a form to configure revision purge settings.
 *
 * @internal
 */
class SettingsForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'revision_purger_settings_form';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return ['revision_purger.settings'];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $interval = $this->config('revision_purger.settings')->get('interval');

    $form['interval'] = [
      '#type' => 'select',
      '#title' => $this->t('Purge on every'),
      '#default_value' => $interval,
      '#description' => $this->t('Purge on every !time !unit'),
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $this->config('revision_purger.settings')
      ->set('interval', $form_state->getValue('interval'))
      ->save();

    parent::submitForm($form, $form_state);
  }

}
