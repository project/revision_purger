<?php

namespace Drupal\revision_purger\Entity;

use Drupal\Core\Config\Entity\ConfigEntityBase;

/**
 * Defines the revision purger entity.
 *
 * @ConfigEntityType(
 *   id = "revision_purger",
 *   label = @Translation("Revision purger"),
 *   label_singular = @Translation("revision purger"),
 *   label_plural = @Translation("revision purgers"),
 *   label_count = @PluralTranslation(
 *     singular = "@count revision purger",
 *     plural = "@count revision purgers",
 *   ),
 *   handlers = {
 *     "form" = {
 *       "default" = "Drupal\revision_purger\Form\RevisionPurgerForm",
 *       "add" = "Drupal\revision_purger\Form\RevisionPurgerForm",
 *       "edit" = "Drupal\revision_purger\Form\RevisionPurgerForm",
 *       "delete" = "Drupal\revision_purger\Form\Form\RevisionPurgerDeleteForm"
 *     },
 *     "list_builder" = "Drupal\revision_purger\RevisionPurgerListBuilder"
 *   },
 *   admin_permission = "administer revision purgers",
 *   config_prefix = "settings",
 *   entity_keys = {
 *     "id" = "id",
 *     "label" = "label"
 *   },
 *   links = {
 *     "delete-form" = "/admin/config/content/revision-purger/manage/{revision_purger}/delete",
 *     "edit-form" = "/admin/config/content/revision-purger/manage/{revision_purger}",
 *     "collection" = "/admin/config/content/revision-purger",
 *   },
 *   config_export = {
 *     "id",
 *     "label",
 *     "target_entity_type_id",
 *     "description",
 *     "count",
 *     "interval",
 *   }
 * )
 */
class RevisionPurger extends ConfigEntityBase  {

  /**
   * The revision purger ID.
   *
   * @var string
   */
  protected $id;

  /**
   * The revision purger label.
   *
   * @var string
   */
  protected $label;

  /**
   * The description of the revision purger.
   *
   * @var string
   */
  protected $description;

  /**
   * The target entity type.
   *
   * @var string
   */
  protected $target_entity_type_id;

  /**
   * The number of revisions.
   *
   * @var integer
   */
  protected $count;


  /**
   * The revision created.
   *
   * @var integer
   */
  protected $interval;


  /**
   * {@inheritdoc}
   */
  public function getDescription() {
    return $this->description;
  }

  /**
   * {@inheritdoc}
   */
  public function setDescription($description) {
    $this->description = $description;
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getTargetEntityTypeId() {
    return $this->target_entity_type_id;
  }

  /**
   * {@inheritdoc}
   */
  public function getCount() {
    return $this->count;
  }

  /**
   * {@inheritdoc}
   */
  public function getInterval() {
    return $this->interval;
  }

}
